/// \brief colors IDs
var redColor = 0;
var greenColor = 1;
var blueColor = 2;
var blackColor = 3;
var yellowColor = 4;
var pinkColor = 5;
var purpleColor = 6;
var whiteColor = 7;
var brownColor = 8;
var grayColor = 9;

var colorIDTab = []

function getColorName(colorID){
    if(colorID === redColor)
    {
        return "red"
    }
    if(colorID === greenColor)
    {
        return "green"
    }
    if(colorID === blueColor)
    {
        return "blue"
    }
    if(colorID === blackColor )
    {
        return "black"
    }
    if(colorID === yellowColor)
    {
        return "yellow"
    }
    if(colorID === pinkColor)
    {
        return "pink"
    }
    if(colorID === purpleColor)
    {
        return "purple"
    }
    if(colorID === whiteColor)
    {
        return "white"
    }
    if(colorID === brownColor)
    {
        return "brown"
    }
    if(colorID === grayColor)
    {
        return "gray"
    }
    console.log("Wrong color ID warning: " + myColor)
    return "-1"
}

function getColorID(myColor){
    if(myColor === "red")
    {
        return redColor
    }
    if(myColor === "green")
    {
        return greenColor
    }
    if(myColor === "blue")
    {
        return blueColor
    }
    if(myColor === "black")
    {
        return blackColor
    }
    if(myColor === "yellow")
    {
        return yellowColor
    }
    if(myColor === "pink")
    {
        return pinkColor
    }
    if(myColor === "purple")
    {
        return purpleColor
    }
    if(myColor === "white")
    {
        return whiteColor
    }
    if(myColor === "brown")
    {
        return brownColor
    }
    if(myColor === "gray")
    {
        return grayColor
    }
    console.log("color not defined warning: " + myColor)
    return -1
}

/// \brief tab is used to count each color object count
var countTab = [0,0,0,0,0,0,0,0,0,0];

/// \brief Function draws and returns available color
function setFrameColor(colorTab){

    var chosenColor = "";

    while(true){
        var choice = parseInt(Math.random()*10);

        if(countTab[choice]<10){
            ++countTab[choice];
            chosenColor = colorTab[choice];
            colorIDTab.push(getColorID(chosenColor))
            break;
        }
    }//while loop

    return chosenColor;
}

/// \brief Function read json file with color configuration and starts createBoard function
function readConfigFileAndSetTheTable(listModel, boardSize){
    var txtFile = Qt.resolvedUrl("color.json");
    var req = new XMLHttpRequest();
    req.open("GET", txtFile, true);
    req.send(null);
    req.onreadystatechange = function(){
        if (req.readyState == 4)
        {
            createBoard(listModel,boardSize,req.responseText);
        }
    }
}
/// \brief Function sets board
function createBoard(listModel, boardSize, colorConfig){

    var colorTab = JSON.parse(colorConfig);

    listModel.clear();

    for(var i = 0; i < boardSize; ++i) {
        listModel.append({"colorValue": setFrameColor(colorTab),
                             "frameID": i,
                             "showTime": getShowTime(),
                             "tileColorID": colorIDTab[i]
                         });
    }
}

/// \brief Function return random show time value
function getShowTime(){
    return (Math.random()*1000)
}
