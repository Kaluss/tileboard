import QtQuick 2.0

ListModel {
    ListElement {
        name: "Red"
        colorID: 0
    }
    ListElement {
        name: "Green"
        colorID: 1
    }
    ListElement {
        name: "Blue"
        colorID: 2
    }
    ListElement {
        name: "Black"
        colorID: 3
    }
    ListElement {
        name: "Yellow"
        colorID: 4
    }
    ListElement {
        name: "Pink"
        colorID: 5
    }
    ListElement {
        name: "Purple"
        colorID: 6
    }
    ListElement {
        name: "White"
        colorID: 7
    }
    ListElement {
        name: "Brown"
        colorID: 8
    }
    ListElement {
        name: "Gray"
        colorID: 9
    }
}
