import QtQuick 2.0

Flipable{
    id: myTile
    width: myBoard.frameSize
    height: myBoard.frameSize

    property bool flipped: false
    property int  tileID: frameID
    property int colorID: tileColorID
    property int  frameShowTime: showTime
    property bool shakeIT: true


    Component.onCompleted: {
        if(myBoard.instantShow){
            myTileFront.scale = 1.0
        }
        else
        {
            showTimer.start()
        }
    }

    function setNormalSize(){
        myTileFront.scale = 1.0
        myTile.z = 0
    }

    function setFlipped(which){
        if(which === myTile.tileID){
            myTile.z = 1000
            myTile.flipped = !myTile.flipped
        }
        else{
            setNormalSize();
            myTile.flipped = false
        }
    }

    Connections{
        target : myGridView
        onWhichChanged :{
            setFlipped(myGridView.which)
        }
    }

    Timer{
        id: showTimer
        interval: myTile.frameShowTime
        repeat: false
        onTriggered: {
            myAnimation.running = true
        }
    }

    Timer{
        id: shakeTimer
        interval: 70
        repeat: true
        onTriggered: {
            if(myTile.shakeIT){
                myAnimationShakeDown.running = true;
            }
            else{
                myAnimationShakeUp.running = true;
            }
            myTile.shakeIT = !myTile.shakeIT
        }
    }
    Timer{
        id: movieTimer
        interval: 2000
        repeat: false
        onTriggered: {
            shakeTimer.stop();
            myAnimationShakeUp.running = true

            background.isMoviePlaying = true
            videoPlayer.source = "filmcount.avi"
            videoPlayer.play()
        }
    }

    NumberAnimation {
    id: myAnimation
    target: myTileFront; properties: "scale"
    from: 0.0; to: 1.0; duration: 700
    }

    NumberAnimation {
    id: myAnimationShakeUp
    target: myTileFront; properties: "scale"
    from: 0.8; to: 1.0; duration: 100
    }

    NumberAnimation {
    id: myAnimationShakeDown
    target: myTileFront; properties: "scale"
    from: 1.0; to: 0.8; duration: 100
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    front: Rectangle{
        id: myTileFront
        color: colorValue
        anchors.fill: myTile
        enabled: !myTile.flipped
        scale: 0

        MouseArea{
            anchors.fill: parent

            onPressAndHold: {
                shakeTimer.start()
                movieTimer.start()
            }

            onReleased: {
                if(shakeTimer.running){
                    movieTimer.stop()
                    shakeTimer.stop()
                    myAnimationShakeUp.running = true
                    return;
                }

                if(background.isMoviePlaying){
                    return;
                }

                if(myGridView.which === myTile.tileID)
                {
                    myGridView.which = -1
                }
                myBoard.flipFrames(myTile.tileID)                
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    back: Rectangle{
        id: myTileBack
        color: colorValue
        anchors.fill: myTile
        enabled: myTile.flipped

        function setNormalSize(){
            myTileFront.scale = 1.0
            myTile.z = 0
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                myTile.flipped = false                
            }
        }
        Rectangle
        {
            id: listContainer
            width: 100
            height: 150
            anchors.centerIn: parent
            color: parent.color

            Component.onCompleted: {
                myListView.currentIndex = -1
            }

            ListView{
                id: myListView
                anchors.fill: parent
                Component {
                    id: contactsDelegate
                    Rectangle {
                        id: wrapper
                        width: 100
                        height: 15
                        color: ListView.isCurrentItem ? "lightsteelblue" : "transparent"
                        Text {
                            id: contactInfo
                            anchors.centerIn: parent
                            text: name
                            color: wrapper.ListView.isCurrentItem ? "red" : "lightsteelblue"
                        }
                        MouseArea{
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                myBoard.removeFrames(colorID)
                            }
                            onEntered: {
                                myListView.currentIndex = index
                            }
                            onExited: {
                                myListView.currentIndex = -1
                            }
                        }
                    }
                }

                model: MyDelegate{}
                delegate: contactsDelegate
                highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
                focus: false
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    transform: Rotation{
        id: myRotation
        origin.x: myTile.front.width/2
        origin.y: myTile.front.height/2
        axis.x: 1.5; axis.y: 0.0; axis.z: 0.0
        angle: 0
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    transitions: [
        Transition{
            SequentialAnimation{
                NumberAnimation {
                    target: myTile; property: "scale"; easing.type: Easing.InBack; duration: 700;                    
                }                
                NumberAnimation{
                    target: myRotation; property: "angle"; easing.type: Easing.InOutBack; duration: 1500
                }                
            }            
        }
    ]

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    states: [
        State{
            name: "backFlipped"
            when: myTile.flipped
            PropertyChanges {
                target: myRotation
                angle: 180
            }
            PropertyChanges {
                target: myTile
                scale: 1.5                
            }
        },
        State{
            name: "frontFlipped"
            when: !myTile.flipped
            PropertyChanges {
                target: myRotation
                angle: 0
            }
        }
    ]

}

