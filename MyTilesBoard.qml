import QtQuick 2.0
import QtMultimedia 5.0
import "MyFunctions.js" as MyFunctions

Rectangle{
    id: background
    width: 800
    height: 600
    color: "light gray"

    property bool stopMovie: false
    property bool isMoviePlaying: false

    MouseArea{
        anchors.fill: parent
        onClicked: {
            myBoard.flipFrames(-1)
            myBoard.showAllFrames()
            background.stopMovie = true
            videoPlayer.stop();
        }
    }

    Rectangle {
      id: myBoard
      anchors.centerIn: parent
      width: background.width - 200
      height: background.height - 200
      color: background.color

      property int frameSize: 192
      property bool instantShow: false
      property bool isTailHidden: false

      Component.onCompleted: {
          MyFunctions.readConfigFileAndSetTheTable(boardModel,100)
          initialShowTimer.start()
      }

      Timer{
          id: initialShowTimer
          interval: 1000
          repeat: false
          onTriggered: {
              myBoard.instantShow = true
          }

      }

      ListModel {
          id: boardModel
       }

      GridView{
         id: myGridView
         anchors.fill: myBoard
         model: boardModel
         cellHeight: myBoard.frameSize
         cellWidth: myBoard.frameSize
         property int which : -1
         property int whichColor: -1
         delegate: SingleTile{}
         add: Transition {
             NumberAnimation { properties: "x,y"; from: 0; duration: 500 }
         }
         remove: Transition {
             ParallelAnimation {
                 NumberAnimation { property: "opacity"; to: 0; duration: 300 }
             }
         }
      }
      Video{
          id: videoPlayer
          anchors.fill: parent

          onPlaying: {
                background.stopMovie = false
          }

          onStopped: {
              if(background.stopMovie){
                  background.stopMovie = false;
                  background.isMoviePlaying = false
                  videoPlayer.source = ""
                  return
              }

              videoPlayer.source = "Sweety.wmv"
              videoPlayer.play()
          }
      }

      function flipFrames(which){
          myGridView.which = which
      }
      function removeFrames(colorID){

          myBoard.isTailHidden = true;

          boardModel.clear();

          for(var tileIndex=0;tileIndex<100;++tileIndex){
              if(colorID === MyFunctions.colorIDTab[tileIndex]){
                  boardModel.append({"colorValue": MyFunctions.getColorName(MyFunctions.colorIDTab[tileIndex]),
                                     "frameID": tileIndex,
                                     "showTime": 0,
                                     "tileColorID": MyFunctions.colorIDTab[tileIndex]
                                    });
              }
          }
      }
      function showAllFrames(){

          if(!myBoard.isTailHidden){
              return;
          }

          boardModel.clear();
          for(var tileIndex=0;tileIndex<100;++tileIndex){
              boardModel.append({"colorValue": MyFunctions.getColorName(MyFunctions.colorIDTab[tileIndex]),
                                 "frameID": tileIndex,
                                 "showTime": 0,
                                 "tileColorID": MyFunctions.colorIDTab[tileIndex]
                                });
          }
          myBoard.isTailHidden=false
      }
    }//myBoard Rectangle
}//background Rectangle
